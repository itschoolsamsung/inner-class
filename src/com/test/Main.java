package com.test;

public class Main {

	//статический внутренний (вложенный) класс
	public static class Inner {
		public static void run() {
			System.out.println("I'm inner static class");
		}
	}

	public static void main(String[] args) {
		//вызываем метод run() статического внутреннего класса
		Inner.run();

		//создаём экземпляр класса, получаем экземпляр его внутреннего класса-члена и вызываем у него метод run()
		Test test = new Test();
		test.getInnerClass().run();

		//создаём экземпляр класса-члена через обращение к экземпляру его внешнего класса и вызываем метод run()
		Test.Inner2 inner2 = test.new Inner2();
		inner2.run();

		//вызываем метод, внутри которого создаётся локальный внутренний класс и вызывается его метод run()
		test.runInner3();

		//создаём экземпляр анонимного класса, наследующего абстрактный класс, и вызываем его метод run()
		new Anon() {

			@Override
			public void run() {
				System.out.println("I'm anonymous class");
			}

		}.run();
	}

}
