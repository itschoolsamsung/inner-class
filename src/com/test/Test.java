package com.test;

public class Test {

	//внутренний класс-член
	public class Inner2 {
		public void run() {
			System.out.println("I'm inner member class");
		}
	}

	public Inner2 getInnerClass() {
		//возвращаем экземпляр класса-члена
		return new Inner2();
	}

	public void runInner3() {
		//локальный внутренний класс
		class Inner3 {
			public void run() {
				System.out.println("I'm inner local class");
			}
		}

		//создаём экземпляр локального внутреннего класса и вызываем метод run()
		new Inner3().run();
	}

}
